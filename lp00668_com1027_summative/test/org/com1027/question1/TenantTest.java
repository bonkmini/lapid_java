package org.com1027.question1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TenantTest {
	
	@Test
	public void testGetAge() {
		Tenant tenant = new Tenant("Lapid", "Pongcharoenyong", 18, TenantType.STUDENT);
		assertEquals(18, tenant.getAge());
	}
	
	@Test
	public void testGetType() {
		Tenant tenant = new Tenant("Lapid", "Pongcharoenyong", 18, TenantType.STUDENT);
		assertEquals(TenantType.STUDENT, tenant.getType());
	}
	
	@Test
	public void testToString() {
		Tenant tenant = new Tenant("Lapid", "Pongcharoenyong", 18, TenantType.STUDENT);
		assertEquals("Lapid Pongcharoenyong", tenant.toString());
	}
}
