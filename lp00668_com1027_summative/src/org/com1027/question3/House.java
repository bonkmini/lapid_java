package org.com1027.question3;

public class House extends Property {
	
	public House(int houseNumber, String street, String city, String postCode, int numberOfRooms) {
		super(houseNumber, street, city, postCode, numberOfRooms);
	}
	
	@Override
    public String displayOccupiedProperty() {
		StringBuffer list = new StringBuffer(this.toString() + "\n");
		double price = 0;
		for(Room i : rooms.keySet()) {
			list.append("Room: " + i.getPrice() + "\n");
			price += i.getPrice();
		}
		list.append("Total: �" + price*12);
		return list.toString();
	}
	
	@Override
	public void occupyRoom(Room room, ITenant tenant) {
		this.rooms.put(room, tenant);
	}
	
	@Override
	public boolean isAvailable() {
		if (getAvailableRooms() == 0) {
			return false;
		}
		else {
			return true;
		}
		
	}

	public String toString() {
		return super.toString() + "house :" + this.getAvailableRooms() + " available)";
	}

}
