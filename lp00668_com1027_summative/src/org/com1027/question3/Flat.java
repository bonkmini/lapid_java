package org.com1027.question3;

public class Flat extends Property{
	
	private int floor = 0;
	
	public Flat(int houseNumber, String city, String street, String postCode, int numberOfRooms, int floor) throws IllegalArgumentException {
		super(houseNumber, city, street, postCode, numberOfRooms);
		this.floor = floor;
	}
	
	@Override
    public String displayOccupiedProperty() {
		StringBuffer list = new StringBuffer(this.toString() + "\n");
		double price = 0;
		for(Room i : rooms.keySet()) {
			list.append("Room: " + i.getPrice() + "\n");
			price += i.getPrice();
		}
		list.append("Total: �" + ((price*12) + 500));
		return list.toString();
	}
	
	@Override
	public void occupyRoom(Room room, ITenant tenant) {
		if (getAvailableRooms() != 0) {
		this.rooms.put(room, tenant);
		}
		else {
			throw new IllegalArgumentException("No room available");
		}
	}
		
	
	@Override
	public boolean isAvailable() {
		if (getAvailableRooms() == 0) {
			return false;
		}
		else {
			return true;
		}
		
	}

	public String toString() {
		return super.toString() + "flat on " + this.floor + " floor :" + this.getAvailableRooms() + " available)";
	}

}
