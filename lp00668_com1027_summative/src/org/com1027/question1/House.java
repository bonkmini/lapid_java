package org.com1027.question1;

import java.util.HashMap;
import java.util.Map;

public class House {
	
	private int houseNumber = 0;
	private String street = null;
	private String city = null;
	private String postCode = null;
	private int numberOfRooms = 0;
	private Map<Room, ITenant> rooms = new HashMap<Room, ITenant>();
	
	public House(int houseNumber, String street, String city, String postCode, int numberOfRooms) throws IllegalArgumentException {
		super();
		this.houseNumber = houseNumber;
		this.street = street;
		this.numberOfRooms = numberOfRooms;
		if (validateCity(city)) {
			this.city = city;
		}
		else {
			throw new IllegalArgumentException("This city name is invalid");
		}
		
		if (validatePostCode(postCode)) {
			this.postCode = postCode;
		}
		else {
			throw new IllegalArgumentException("This post code is invalid");
		}
	}
	
	public int getAvailableRooms() {
		return this.numberOfRooms - this.rooms.size();
	}
	
	public double getPrice() {
		double result = 0;
		for (Room room : rooms.keySet()) {
			result += room.getPrice();
		}
		return result;
	}
	
	public boolean isAvailable() {
		if (getAvailableRooms() == 0) {
			return false;
		}
		else {
			return true;
		}   
	}
	
	public void occupyRoom(Room room, ITenant tenant) {
		this.rooms.put(room, tenant);
	}
	
	@Override
	public String toString() {
		String avi = (isAvailable()) ? "available" : "Unavailable";
		return houseNumber + " " + street + ", " + city + " " + postCode + " (" + numberOfRooms + " bedroom house " + ":" + getAvailableRooms() + " " + avi + ")";
	}
	
	private boolean validateCity(String city) {
		String valid = "[A-Za-z]+";
		if (city.matches(valid)) {
			return true;
		}
		return false;

	}
	
	private boolean validatePostCode(String postCode) {
		String valid1 = "[A-Z]{2}[0-9][\\s][0-9][A-Z]{2}";
		if (postCode.matches(valid1)) {
			return true;
		}
		return false;
	}
	
	
	
	
	

}
