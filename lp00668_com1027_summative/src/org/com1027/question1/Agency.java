package org.com1027.question1;

public class Agency {
	
	private String name = null;
	private String phoneNumber = null;
	
	public Agency(String name, String phoneNumber) throws IllegalArgumentException {
		super();
		if(name.matches("[A-Z][a-z]+[\\s][A-Z][a-z]+")) {
			this.name = name;
		}
		else {
			throw new IllegalArgumentException("This name is not valid");
		}
		
		if(phoneNumber.matches("[0-9]{5}[\\s][0-9]{6}")) {
			this.phoneNumber = phoneNumber;
		}
		else {
			throw new IllegalArgumentException("This number is not valid");
		}
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getPhoneNumber() {
		return this.phoneNumber;
	}
	
	public String toString() {
		return "Agency: " + getName() + ", Phone Number: " + getPhoneNumber(); 
	}

}
