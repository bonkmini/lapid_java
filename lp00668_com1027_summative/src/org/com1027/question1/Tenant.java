package org.com1027.question1;

public class Tenant implements ITenant {
	
	private String name = null;
	private String surename = null;
	private int age = 0;
	private TenantType type = null;
	
	public Tenant(String name, String surename, int age, TenantType type) {
		super();
		this.name = name;
		this.surename = surename;
		this.age = age;
		this.type = type;
		
	}
	
	public int getAge() {
		return this.age;
	}

	@Override
	public TenantType getType() {
		return type;
	}
	
	@Override
	public String toString() {
		return name + " " + surename;
	}

}
