package org.com1027.question1;

import java.util.ArrayList;
import java.util.List;
/**
 * This class show all the properties for a specific letting agency.
 */
public class PropertyManagement {
	
	/**
	 * Estate agency that use the function in Agency class
	 */
	private Agency estateAgency = null;
	
	/**
	 * Create an ArrayList for House, so we can add properties in it
	 */
	private List<House> properties = new ArrayList<House>();
	
	/**
	 * The special method that initialise the field within this class
	 * @param estateAgency: This is the field that has been initialised 
	 */
	public PropertyManagement(Agency estateAgency) {
		super();
		this.estateAgency = estateAgency;
	}
	
	/**
	 * This method is used to add objects into the ArrayList
	 * @param property: The variable that represent the all the properties
	 */
	public void addProperty(House property) {
		this.properties.add(property);
	}
	
	/**
	 * This method is used to add the occupied room into the property 
	 * @param room: The room that has been occupied
	 * @param tenant: The tenant that has been assigned to a specific room
	 */
	public void addTenant(House property, Room room, Tenant tenant) {
		property.occupyRoom(room, tenant);
	}
	
	/**
	 * This method is used to display all the objects that have been added to the ArrayList
	 * @return Display the objects that has been added to the ArrayList in String
	 */
	public String displayProperties() {
		String result = "";
		//To show if the objects can be added into ArrayList or not
		for(int i = 0; i < this.properties.size(); i++) {
		result += this.properties.get(i).toString() + "\n";
		}
		return result;
	}
	
	/**
	 * This method is used to show the information of estate agency
	 * @return Return the information of estate agency
	 */
	public String getEstateAgency() {
		return this.estateAgency.toString();
	}
	
	/**
	 * This method is used to remove the objects from ArrayList
	 * @throws IllegalArgumentException: To prevent the user from removing the object
	 * that does not exist in the ArrayList
	 */
	public void removeProperty(House property) throws IllegalArgumentException {
		// To check and remove the existing objects in ArrayList
		if(properties.contains(property)) {
			this.properties.remove(property);
		}
		else {
			throw new IllegalArgumentException("The required property does not exist");
		}
		
	}
		

		
}
