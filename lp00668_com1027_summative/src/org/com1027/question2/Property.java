package org.com1027.question2;

import java.util.HashMap;
import java.util.Map;

public abstract class Property {
	
	private int houseNumber = 0;
	private String street = null;
	private String city = null;
	private String postCode = null;
	private int numberOfRooms = 0;
	protected Map<Room, ITenant> rooms = new HashMap<Room, ITenant>();
	
	public Property(int houseNumber, String street, String city, String postCode, int numberOfRooms) throws IllegalArgumentException {
		super();
		this.houseNumber = houseNumber;
		this.street = street;
		this.numberOfRooms = numberOfRooms;
		if (validateCity(city)) {
			this.city = city;
		}
		else {
			throw new IllegalArgumentException("This city name is invalid");
		}
		
		if (validatePostCode(postCode)) {
			this.postCode = postCode;
		}
		else {
			throw new IllegalArgumentException("This post code is invalid");
		}
	}
	
	public int getAvailableRooms() {
		return this.numberOfRooms - this.rooms.size();
	}
	
	public double getPrice() {
		double result = 0;
		for (Room room : rooms.keySet()) {
			result += room.getPrice();
		}
		return result;
	}
	
	public abstract boolean isAvailable();
	
	public abstract void occupyRoom(Room room, ITenant tenant);
	
	@Override
	public String toString() {
		return houseNumber + " " + street + ", " + city + " " + postCode + " (" + numberOfRooms + " bedroom ";
	}
	
	public abstract String displayOccupiedProperty();
	
	private boolean validateCity(String city) {
		String valid = "[A-Za-z]+";
		if (city.matches(valid)) {
			return true;
		}
		return false;

	}
	
	private boolean validatePostCode(String postCode) {
		String valid1 = "[A-Z]{2}[0-9][\\s][0-9][A-Z]{2}";
		if (postCode.matches(valid1)) {
			return true;
		}
		return false;
	}
	
	
	
	
	

}
