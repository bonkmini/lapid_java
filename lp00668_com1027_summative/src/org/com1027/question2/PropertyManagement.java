package org.com1027.question2;

import java.util.ArrayList;
import java.util.List;

public class PropertyManagement {
	
	
	private Agency estateAgency = null;
	
	
	private List<Property> properties = new ArrayList<Property>();
	
	
	public PropertyManagement(Agency estateAgency) {
		super();
		this.estateAgency = estateAgency;
	}
	
	
	public void addProperty(Property property) {
		this.properties.add(property);
	}
	
	public void addTenant(Property property, Room room, Tenant tenant) {
		property.occupyRoom(room, tenant);
	}
	
	public String displayProperties() {
		String result = "";
		for(int i = 0; i < this.properties.size(); i++) {
		result += this.properties.get(i).toString() + "\n";
		}
		return result;
	}
	
	
	public String getEstateAgency() {
		return this.estateAgency.toString();
	}
	
	public void removeProperty(Property property) throws IllegalArgumentException {
		if(properties.contains(property)) {
			this.properties.remove(property);
		}
		else {
			throw new IllegalArgumentException("The required property does not exist");
		}
		
	}
		

		
}
