package Lapid_cw;

import java.sql.*;
import java.util.*;

public class Unsold_products_report implements IUnsold {
    
	private Connection connect;
    private Statement statement;
    private Map<String, Integer> sumQuantityOrdered = new HashMap<String, Integer>();

    public Unsold_products_report() {
        super();
        this.connect = null;
        this.statement = null;
        this.openConn();
    }

    @Override
    public List<Products> getProducts() {
        ArrayList<Products> products = new ArrayList<Products>();
        try {
            String query = "SELECT * FROM products ORDER BY products.productCode ASC";

            ResultSet results = this.statement.executeQuery(query);
            while (results.next()) {
                String productCode = results.getString("productCode");
                String productName = results.getString("productName");
                String productLine = results.getString("productLine");
                String productScale = results.getString("productScale");
                String productVendor = results.getString("productVendor");
                String productDescription = results.getString("productDescription");
                int quantityInStock = results.getInt("quantityInStock");
                double buyPrice = results.getDouble("buyPrice");
                double MSRP = results.getDouble("MSRP");
                products.add(new Products(productCode, productName,
                        productLine, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP));

            }

        } catch (SQLException e) {
            System.out.println("SQLException happened while retrieving records - abort programmme");
            throw new RuntimeException(e);
        }
        return products;
    }

    @Override
    public List<OrderDetails> getOrderDetails() {
        ArrayList<OrderDetails> orderDetails = new ArrayList<OrderDetails>();
        try {
            String query = "SELECT * FROM orderDetails ORDER BY orderDetails.productCode ASC";

            ResultSet results = this.statement.executeQuery(query);

            while (results.next()) {
                int orderNumber = results.getInt("orderNumber");
                String productCode = results.getString("productCode");
                int quantityOrdered = results.getInt("quantityOrdered");
                double priceEach = results.getDouble("priceEach");
                int orderLineNumber = results.getInt("orderLineNumber");
                orderDetails.add(new OrderDetails(orderNumber, productCode, quantityOrdered, priceEach , orderLineNumber));

            }

        } catch (SQLException e) {
            System.out.println("SQLException happened while retrieving records - abort programmme");
            throw new RuntimeException(e);
        }
        return orderDetails;
    }

    @Override
    public void reportUnsoldProducts() {
        List<Products> products = getProducts();
        Iterator<Products> iteratorProducts = products.iterator();

        List<OrderDetails> orderDetails = getOrderDetails();
        Iterator<OrderDetails> iteratorOrderDetails = orderDetails.iterator();


        while (iteratorOrderDetails.hasNext()) {
            OrderDetails tmpOrderDetails = null;
            String currentProductCode = tmpOrderDetails.getProductCode();
            int currentSum = 0;
            for (currentProductCode.equals(tmpOrderDetails.getProductCode());
                 !currentProductCode.equals(tmpOrderDetails.getProductCode());
                 iteratorOrderDetails.next()) {
                currentSum += tmpOrderDetails.getQuantityOrdered();
            }
            sumQuantity(currentProductCode, currentSum);
        }

        while (iteratorProducts.hasNext()) {
            Products tmpProducts = null;
            String currentProductCode = tmpProducts.getProductCode();
            int totalQuantityOrdered = sumQuantityOrdered.get(currentProductCode);
            int remainingQuantityInStock = tmpProducts.getQuantityInStock();
            int calculateQuantityInStock = remainingQuantityInStock - totalQuantityOrdered;

            if (calculateQuantityInStock > 0){
                System.out.println("Product Code: " + tmpProducts.getProductCode());
                System.out.println("Product Name: " + tmpProducts.getProductName());
                System.out.println("Product Line: " + tmpProducts.getProductLine());
                System.out.println("Product Scale: " + tmpProducts.getProductScale());
                System.out.println("Product Vendor: " + tmpProducts.getProductVendor());
                System.out.println("Product Description: " + tmpProducts.getProductDescription());
                System.out.println("Remaining Quantity In Stock" + calculateQuantityInStock);
                System.out.println("Buy Price: " + tmpProducts.getBuyPrice());
                System.out.println("MSRP: " + tmpProducts.getMSRP());
            }
            iteratorProducts.next();

        }

    }

    public void sumQuantity(String currentProductCode, int currentSum) {
        this.sumQuantityOrdered.put(currentProductCode, currentSum);
    }

    @Override
    public void openConn() {
        try {
            if (this.connect == null || this.connect.isClosed()) {
                this.connect = DriverManager
                        .getConnection(
                                "jdbc:hsqldb:file:db_data/dataFilestore;ifexists=true;shutdown=true",
                                "SA", "");
            }
            if (this.statement == null || this.statement.isClosed()) {
                this.statement = this.connect.createStatement();
            }

        } catch (SQLException e) {
            System.out
                    .println("Cannot Connect to the databases");
            throw new RuntimeException(e);
        }
    }

    @Override
    public void closeConn() {

        try {

            if (this.statement != null) {
                this.statement.close();
            }
            if (this.connect != null) {
                this.connect.close();
            }
            System.out.println("The connection has been closed");
        } catch (Exception e) {
            System.out
                    .print("Cannot close the databases");
            throw new RuntimeException(e);
        }
    }

}
