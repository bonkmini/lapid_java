package Lapid_cw;

public class OrderDetails {

    private int orderNumber;
    private String productCode;
    private int quantityOrdered;
    private double priceEach;
    private int orderLineNumber;

    public OrderDetails(int orderNumber, String productCode, int quantityOrdered, double priceEach, int orderLineNumber) {
        super();
        this.orderNumber = orderNumber;
        this.productCode = productCode;
        this.quantityOrdered = quantityOrdered;
		this.priceEach = priceEach;
        this.orderLineNumber = orderLineNumber;
    }

    public int getOrderNumber() {
        return this.orderNumber;
    }

    public String getProductCode() {
        return this.productCode;
    }

    public int getQuantityOrdered() {
        return this.quantityOrdered;
    }

    public double getPriceEach() {
		return this.priceEach;
	}

    public int getOrderLineNumber() {
        return this.orderLineNumber;
    }

}
