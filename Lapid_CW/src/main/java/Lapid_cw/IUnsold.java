package Lapid_cw;

import java.util.List;

public interface IUnsold {

    public List<Products> getProducts();

    public List<OrderDetails> getOrderDetails();

    public void reportUnsoldProducts();

    public void openConn();

    public void closeConn();
}
