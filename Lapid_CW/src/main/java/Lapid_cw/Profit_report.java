package Lapid_cw;

import java.sql.*;
import java.util.*;

public class Profit_report implements IProfit {
    
	private Connection connect;
    private Statement statement;
    private Map<String, Integer> sumQuantityOrdered = new HashMap<String, Integer>();
    private Map<String, Double> sumQuantityOrderedPrice = new HashMap<String, Double>();
    private Map<String, Double> productProfit = new HashMap<String, Double>();

    public Profit_report() {
        super();
        this.connect = null;
        this.statement = null;
        this.openConn();
    }

    @Override
    public List<Products> getProducts() {
        ArrayList<Products> products = new ArrayList<Products>();
        try {
            String query = "SELECT * FROM products ORDER BY products.productCode ASC";

            ResultSet results = this.statement.executeQuery(query);
            
            while (results.next()) {
                String productCode = results.getString("productCode");
                String productName = results.getString("productName");
                String productLine = results.getString("productLine");
                String productScale = results.getString("productScale");
                String productVendor = results.getString("productVendor");
                String productDescription = results.getString("productDescription");
                int quantityInStock = results.getInt("quantityInStock");
                double buyPrice = results.getDouble("buyPrice");
                double MSRP = results.getDouble("MSRP");
                products.add(new Products(productCode, productName,
                        productLine, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP));

            }

        } catch (SQLException e) {
            System.out.println("SQLException happened while retrieving records - abort programmme");
            throw new RuntimeException(e);
        }
        return products;
    }

    @Override
    public List<ProductLines> getProductLines() {
        ArrayList<ProductLines> productLines = new ArrayList<ProductLines>();
        try {
            String query = "SELECT * FROM productLines";

            ResultSet results = this.statement.executeQuery(query);
            
            while (results.next()) {
                String productLine = results.getString("productLine");
                String textDescription = results.getString("textDescription");
                String htmlDescription = results.getString("htmlDescription");
                Blob image = results.getBlob("image");
                productLines.add(new ProductLines(productLine, textDescription, htmlDescription, image));

            }

        } catch (SQLException e) {
            System.out.println("SQLException happened while retrieving records - abort programmme");
            throw new RuntimeException(e);
        }
        return productLines;
    }

    @Override
    public List<OrderDetails> getOrderDetails() {
        ArrayList<OrderDetails> orderDetails = new ArrayList<OrderDetails>();
        try {
            String query = "SELECT * FROM orderDetails ORDER BY orderDetails.productCode ASC";

            ResultSet results = this.statement.executeQuery(query);

            while (results.next()) {
                int orderNumber = results.getInt("orderNumber");
                String productCode = results.getString("productCode");
                int quantityOrdered = results.getInt("quantityOrdered");
                double priceEach = results.getDouble("priceEach");
                int orderLineNumber = results.getInt("orderLineNumber");
                orderDetails.add(new OrderDetails(orderNumber, productCode, quantityOrdered, priceEach, orderLineNumber));

            }

        } catch (SQLException e) {
            System.out.println("SQLException happened while retrieving records - abort programmme");
            throw new RuntimeException(e);
        }
        return orderDetails;
    }

    @Override
    public void reportProfit() {
        List<Products> products = getProducts();
        Iterator<Products> iteratorProducts = products.iterator();

        List<ProductLines> productLines = getProductLines();
        Iterator<ProductLines> iteratorProductsLines = productLines.iterator();

        List<OrderDetails> orderDetails = getOrderDetails();
        Iterator<OrderDetails> iteratorOrderDetails = orderDetails.iterator();


        while (iteratorOrderDetails.hasNext()) {
            OrderDetails tmpOrderDetails = null;
            String currentProductCode = tmpOrderDetails.getProductCode();
            int currentQuantitySum = 0;
            double currentPriceSum = 0d;
            for (currentProductCode.equals(tmpOrderDetails.getProductCode());
                 !currentProductCode.equals(tmpOrderDetails.getProductCode());
                 iteratorOrderDetails.next()) {
                currentQuantitySum += tmpOrderDetails.getQuantityOrdered();
                currentPriceSum += tmpOrderDetails.getQuantityOrdered() * tmpOrderDetails.getPriceEach();
            }
            sumQuantity(currentProductCode, currentQuantitySum);
            sumPrice(currentProductCode, currentPriceSum);
        }

        while (iteratorProducts.hasNext()) {
            Products tmpProducts = null;
            String currentProductCode = tmpProducts.getProductCode();
            String currentProductLine = tmpProducts.getProductLine();
            double totalProductBuyPrice = sumQuantityOrdered.get(currentProductCode) * tmpProducts.getBuyPrice();
            double productProfit = sumQuantityOrderedPrice.get(currentProductCode) - totalProductBuyPrice;
            sumProductLinePrice(currentProductLine, productProfit);
            iteratorProducts.next();
        }

        while (iteratorProductsLines.hasNext()) {
            ProductLines tmpProductLines = null;
            System.out.println("Product Line: " + tmpProductLines.getProductLine());
            System.out.println("Product Line Profit: " + this.productProfit.get(tmpProductLines.getProductLine()));
            iteratorProducts.next();        
        }

    }

    public void sumQuantity(String currentProductCode, int currentQuantitySum) {
        this.sumQuantityOrdered.put(currentProductCode, currentQuantitySum);
    }

    public void sumPrice(String currentProductCode, double currentPriceSum) {
        this.sumQuantityOrderedPrice.put(currentProductCode, currentPriceSum);
    }

    public void sumProductLinePrice(String currentProductLine, double productProfit) {
        try {
            this.productProfit.put(currentProductLine, this.productProfit.get(currentProductLine) + productProfit);
        } catch (Exception e) {
            this.productProfit.put(currentProductLine, productProfit);
        }
    }

    @Override
    public void openConn() {
        try {
            if (this.connect == null || this.connect.isClosed()) {
                this.connect = DriverManager
                        .getConnection(
                                "jdbc:hsqldb:file:db_data/dataFilestore;ifexists=true;shutdown=true",
                                "SA", "");
            }
            if (this.statement == null || this.statement.isClosed()) {
                this.statement = this.connect.createStatement();
            }

        } catch (SQLException e) {
            System.out
                    .println("Cannot Connect to the databases");
            throw new RuntimeException(e);
        }
    }

    @Override
    public void closeConn() {

        try {

            if (this.statement != null) {
                this.statement.close();
            }
            if (this.connect != null) {
                this.connect.close();
            }
            System.out.println("The connection has been closed");
        } catch (Exception e) {
            System.out
                    .print("Cannot close the databases");
            throw new RuntimeException(e);
        }
    }

}
