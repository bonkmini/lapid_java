package Lapid_cw;

public class reportInstances {
	
	private static final IPayments iPayments = new Payments_report();
	private static final IUnsold iUnsold = new Unsold_products_report();
	private static final IProfit iProfit = new Profit_report();
	
	public static IPayments getIPayments() {
		return reportInstances.iPayments;
	}
	
	public static IUnsold getIUnsold() {
		return reportInstances.iUnsold;
	}
	
	public static IProfit getIProfit() {
		return reportInstances.iProfit;
	}

}
