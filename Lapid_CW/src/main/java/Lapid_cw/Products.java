package Lapid_cw;

public class Products {
	
	private String productCode;
	private String productName;
	private String productLine;
	private String productScale;
	private String productVendor;
	private String productDescription;
	private int quantityInStock;
	private double buyPrice;
	private double MSRP;
	
	public Products(String productCode, String productName, String productLine, String productScale, String productVendor,
			String productDescription, int quantityInStock, double buyPrice, double MSRP) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.productLine = productLine;
		this.productScale = productScale;
		this.productVendor = productVendor;
		this.productDescription = productDescription;
		this.quantityInStock = quantityInStock;
		this.buyPrice = buyPrice;
		this.MSRP = MSRP;
	}
	
	public String getProductCode() {
		return this.productCode;
	}
	
	public String getProductName() {
		return this.productName;
	}
	
	public String getProductLine() {
		return this.productLine;
	}
	
	public String getProductScale() {
		return this.productScale;
	}
	
	public String getProductVendor() {
		return this.productVendor;
	}
	
	public String getProductDescription() {
		return this.productDescription;
	}
	
	public int getQuantityInStock() {
		return this.quantityInStock;
	}
	
	public double getBuyPrice() {
		return this.buyPrice;
	}
	
	public double getMSRP() {
		return this.MSRP;
	}

}
