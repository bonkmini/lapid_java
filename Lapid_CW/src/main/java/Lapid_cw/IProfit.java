package Lapid_cw;

import java.util.List;

public interface IProfit {
    
	public List<Products> getProducts();

    public List<ProductLines> getProductLines();

    public List<OrderDetails> getOrderDetails();

    public void reportProfit();

    public void openConn();

    public void closeConn();
}
