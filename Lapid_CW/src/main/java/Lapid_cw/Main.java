package Lapid_cw;

import Lapid_cw.Payments;
import Lapid_cw.reportInstances;

public class Main {

	public static void main(String[] args) {
		IPayments iPayments = null;
		IUnsold iUnsold = null;
		IProfit iProfit = null;
		
		try {
			iPayments = reportInstances.getIPayments();
			System.out.println("------ Report: Payment that exceed $100000 ------" + "/n");
			iPayments.reportPayment();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (iPayments != null) {
				iPayments.closeConn();
			}
		}
	}

}
