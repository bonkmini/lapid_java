package Lapid_cw;

import java.sql.Blob;

public class ProductLines {
	
	private String productLine;
	private String textDescription;
	private String htmlDescription;
	private Blob image;
	
	public ProductLines(String productLine, String textDescription, String htmlDescription, Blob image) {
		super();
		this.productLine = productLine;
		this.textDescription = textDescription;
		this.htmlDescription = htmlDescription;
		this.image = image;
	}
	
	public String getProductLine() {
		return this.productLine;
	}
	
	public String getTextDescription() {
		return this.textDescription;
	}
	
	public String getHtmlDescription() {
		return this.htmlDescription;
	}
	
	public Blob getImage() {
		return this.image;
	}
}
