package Lapid_cw;

import java.util.List;

public interface IPayments {
	
	public List<Payments> getPayments();
	
	public void reportPayment();
	
	public void openConn();
	
	public void closeConn();

}
