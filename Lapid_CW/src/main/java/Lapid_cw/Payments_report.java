package Lapid_cw;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Payments_report implements IPayments {
	
	private Connection connect;
	private Statement statement;
	
	public Payments_report() {
		super();
		this.connect = null;
		this.statement = null;
		this.openConn();	
	}
	
	@Override
	public void openConn() {
		try {
			if (this.connect == null || this.connect.isClosed()) {
				this.connect = DriverManager
						.getConnection(
								"jdbc:hsqldb:file:db_data/dataFilestore;ifexists=true;shutdown=true",
								"SA", "");
			}
			if (this.statement == null || this.statement.isClosed()) {
				this.statement = this.connect.createStatement();
			}

		} catch (SQLException e) {
			System.out
					.println("Cannot Connect to the databases");
			throw new RuntimeException(e);
		}
	}

	@Override
	public void closeConn() {

		try {

			if (this.statement != null) {
				this.statement.close();
			}
			if (this.connect != null) {
				this.connect.close();
			}
			System.out.println("The connection has been closed");
		} catch (Exception e) {
			System.out
					.print("Cannot close the databases");
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public List<Payments> getPayments() {
		ArrayList<Payments> payments = new ArrayList<Payments>();
		try {
			String query = "SELECT * FROM payments";

			ResultSet results = this.statement.executeQuery(query);

			while (results.next()) {
				int customerNumber = results.getInt("customerNumber");
				String checkNumber = results.getString("checkNumber");
				Date paymentDate = results.getDate("paymentDate");
				double amount = results.getDouble("amount");
				payments.add(new Payments(customerNumber, checkNumber, paymentDate, amount));
				
			}
	
		} catch (SQLException e) {
			System.out.println("SQLException happened while retrieving records - abort programmme");
			throw new RuntimeException(e);
		}
		return payments;
	}
	
	@Override
	public void reportPayment() {
		List<Payments> payments = getPayments();
		Iterator<Payments> iterator = payments.iterator();
		
		Payments tmpPayments;
		while(iterator.hasNext()) {
			tmpPayments = iterator.next();
			if(tmpPayments.getAmount() >= 100000d) {
				System.out.println("Customer Number: " + tmpPayments.getCustomerNumber());
				System.out.println("Validation: " + tmpPayments.getCheckNumber());
				System.out.println("Payment Date: " + tmpPayments.getPaymentDate());
				System.out.println("Paid Amount: " + tmpPayments.getAmount());
			}
		}
	}

}
