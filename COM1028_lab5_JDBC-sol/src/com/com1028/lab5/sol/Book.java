/**
 * The POJO for a Book
 * 
 */

package com.com1028.lab5.sol;

public class Book {

	// local field variables
	private String author;
	private String title;
	private int pages;
	private String isbn;

	public Book(String author, String title, int pages, String isbn) {
		// Super here for convention
		super();
		this.author = author;
		this.title = title;
		this.pages = pages;
		this.isbn = isbn;
		// Set the object variables

	}

	public String getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public int getPages() {
		return pages;
	}

	public String getIsbn() {
		return isbn;
	}
}

