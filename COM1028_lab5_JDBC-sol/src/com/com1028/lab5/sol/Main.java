package com.com1028.lab5.sol;

import com.com1028.lab5.sol.dao.BookDAO;
import com.com1028.lab5.sol.dao.DAOFactory;

public class Main {

	public static void main(String[] args) {
		BookDAO bookDAO = null;
		try {
			bookDAO = DAOFactory.getBookDAO();

			Book ai = new Book("Artificial Intelligence", "Norvig", 1132,
					"978-0-13-207148-2");

			System.out.println("What's in the DB atm?");
			bookDAO.showAllBooks();
			System.out.println("Now creating a book");
			bookDAO.writeBook(ai);

			System.out
					.println("Finished storing the book, so what's in the DB now?");

			bookDAO.showAllBooks();
			System.out.println("We are done...");

		} catch (Exception e) {
			// catch any exceptions and log them
			e.printStackTrace();
		} finally {
			// IMPORTANT : now close the connection to the database - DO NOT FORGET TO DO THIS!
			if (bookDAO != null) {
				bookDAO.closeConnection();
			}
		}
	}

}
