package com.com1028.lab5.sol.dao;

public class DAOFactory {
	//singleton instance of the BookDAOImpl;
	
	private static final BookDAO bookDAO = new BookDAOImpl();

	public static BookDAO getBookDAO() {
		return DAOFactory.bookDAO;
	}

}
