package com.com1028.lab5.sol.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.com1028.lab5.sol.Book;

public class BookDAOImpl implements BookDAO {

	// Local Variables
	private Connection connect;
	private Statement statement;

	// Default Constructor
	public BookDAOImpl() {
		super();

		this.connect = null;
		this.statement = null;
		this.openConnection();

	}

	@Override
	public void openConnection() {
		// Note we assume a single threaded application which will only not
		// need multiple connections
		// for performance reasons we create one connection and one
		// statement and re-use them
		// YOU NEED TO REPLACE JUST myTestFilestore WITH THE PATH TO YOUR myDBfilestore
		try {
			// recreate the connection if needed
			if (this.connect == null || this.connect.isClosed()) {
				// change the DB Path
				//
				this.connect = DriverManager
						.getConnection(
								"jdbc:hsqldb:file:db_data/myTestFilestore;ifexists=true;shutdown=true",
								"SA", "");
			}
			//recreate the statement if needed
			if (this.statement == null || this.statement.isClosed()) {
				this.statement = this.connect.createStatement();
			}

		} catch (SQLException e) {
			System.out
					.println("ERRRO - Failed to create a connection to the database");
			throw new RuntimeException(e);
		}
	}

	@Override
	public void closeConnection() {

		try {

			if (this.statement != null) {
				this.statement.close();
			}
			if (this.connect != null) {
				this.connect.close();
			}
			System.out.println("Closed the connection to the database");
		} catch (Exception e) {
			System.out
					.print("ERROR-Failed to close the connection to the database");
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Book> getBooks() {
		ArrayList<Book> books = new ArrayList<Book>();
		try {
			// This is our prepared query, that selects everything from book
			// table
			String query = "SELECT * FROM book";

			// Executes the query and stores the results.
			ResultSet results = this.statement.executeQuery(query);

			while (results.next()) {

				/*
				 * Assign results from query to their own variable. We can
				 * reference columns by their name of index value e.g. 0
				 */
				String title = results.getString("title");
				String author = results.getString("author");
				int pages = results.getInt("pages");
				String isbn = results.getString("isbn");
				books.add(new Book(author, title, pages, isbn));
			}

		} catch (SQLException e) {
			System.out
					.println("SQLException happened while retrieving records- abort programmme");
			throw new RuntimeException(e);
		}
		return books;
	}

	@Override
	public void writeBook(Book book) {
		try {

			// Prepared statements allow us to use variables in them more
			// efficiently
			PreparedStatement preparedStatement = this.connect
					.prepareStatement("INSERT INTO book (title, author, pages, isbn) VALUES (?, ?, ?, ?)");

			preparedStatement.setString(1, book.getTitle());
			preparedStatement.setString(2, book.getAuthor());
			preparedStatement.setInt(3, book.getPages());
			preparedStatement.setString(4, book.getIsbn());

			// This executes the query. Please note there are different execute
			// types.
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.out
					.println("SQLException happened while writing a book- abort programmme");
			throw new RuntimeException(e);
		}

	}

	@Override
	public void showAllBooks() {

		List<Book> books = getBooks();
		Iterator<Book> iter = books.iterator();

		// While there are still results...
		Book tmpBook;
		while (iter.hasNext()) {
			tmpBook = iter.next();
			// Prints results to console
			System.out.println("---- " + tmpBook.getTitle() + " ----");
			System.out.println("Author: " + tmpBook.getAuthor());
			System.out.println("Pages: " + tmpBook.getPages());
			System.out.println("ISBN 13: " + tmpBook.getIsbn() + "\n");
		}
	}

}
