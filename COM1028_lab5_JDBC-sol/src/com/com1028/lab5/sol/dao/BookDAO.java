package com.com1028.lab5.sol.dao;

import java.util.List;

import com.com1028.lab5.sol.Book;

/**
 * Create a Data Access Object for the Books.
 * 
 * @author swesemeyer
 *
 */
public interface BookDAO {
	/**
	 * retrieves all the books from the database. A list of size 0 is returned
	 * if no books are stored.
	 * 
	 * @return List<Book>
	 */
	public List<Book> getBooks();

	/**
	 * persists a book to the database
	 * 
	 * @param book
	 */
	public void writeBook(Book book);

	/**
	 * Print a list of al books to the console
	 */
	public void showAllBooks();

	/**
	 * Open the connection to the database
	 * 
	 */

	public void openConnection();

	/**
	 * Close the connection to the database
	 *
	 * When you're finished with the db its very important that you close the
	 * connections so that data gets persisted.
	 */
	public void closeConnection();

}
